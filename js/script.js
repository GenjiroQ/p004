function ajax_cargar_datos(){

    const http = new XMLHttpRequest;
    const url ="https://jsonplaceholder.typicode.com/photos"
    http.onreadystatechange = function(){
    // Validar la respuesta
    if(this.status == 200 && this.readyState == 4){
                    
        console.log(this.responseText);
        let res = document.getElementById("respuesta");
        res.innerHTML="";

        //Convertir en formato JSON para hacer objetos

        const json = JSON.parse(this.responseText)

        for(const datos of json){

            if(datos.id>100){
                break 
            }

            res.innerHTML+= "<div class='texto'><h1> Album Id: " + datos.albumId + "</h1><br><h2> ID: " + datos.id + "</h2><br><h2> Titulo: " + datos.title 
            + "</h2><br><a  href='"+ datos.url +"'><img src='" + datos.thumbnailUrl + "'></a><br></div>";

        }
    }
}

http.open('GET',url,true);
http.send();

}

document.getElementById("btnCargar").addEventListener("click",function(){

    ajax_cargar_datos();

    });